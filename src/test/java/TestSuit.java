import models.PlateauGrid;
import models.Rover;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestSuit {
    PlateauGrid plateauGrid;

    @Before
    public void init(){
        plateauGrid = PlateauGrid.getPlateauGridSingleton();
        plateauGrid.init(5,5);
    }

    @Test
    public void testTurnRover(){
        assertEquals("W",NavigationRuleEngine.turnRover("N","L"));
        assertEquals("E",NavigationRuleEngine.turnRover("N","R"));
    }

    @Test
    void testExecuteMove(){
        Rover rover = new Rover();
        rover.setX(2);
        rover.setY(2);
        rover.setOrientation("N");

    }

}
