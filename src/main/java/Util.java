import models.PlateauGrid;
import models.Rover;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Util {
    public static void parsePlateauGridInfo(String input){
        String[] splited = input.split(" ");
        PlateauGrid.getPlateauGridSingleton().init(Integer.parseInt(splited[0]),Integer.parseInt(splited[1]));
    }

    /*
    * Rover1 - [X  Y  N][LMMLMMLLL]
    * Rover2 - [X  Y  N][LRMLRLMRL]
    * */
    public static List<Rover> parseRoverInfo(Map<String, String[]> input){
        String[] coordinates;
        ArrayList<Rover> roverList = new ArrayList<Rover>();

        for(Map.Entry<String,String[]> entry: input.entrySet()){
            Rover rover = new Rover();
            coordinates = entry.getValue()[0].split(" ");
            rover.setName(entry.getKey());
            rover.setX(Integer.parseInt(coordinates[0]));
            rover.setY(Integer.parseInt(coordinates[1]));
            rover.setOrientation(coordinates[2]);
            rover.setMoveSequence(entry.getValue()[1]);
            roverList.add(rover);
        }

        return roverList;
    }

}
