import models.PlateauGrid;
import models.Rover;

public class Navigator {
    public static String navigate(PlateauGrid plateauGrid, Rover rover){
        String[] sequence = rover.getMoveSequence().split("");
        for(String item: sequence){
            if(item.equals("M")){
                NavigationRuleEngine.executeMove(rover);
            }else{
                rover.setOrientation(NavigationRuleEngine.turnRover(rover.getOrientation(),item));
            }

            if(!movedWithinGrid(PlateauGrid.getPlateauGridSingleton(),rover)){
                return String.format("[Moving] %s - [Danger] out of the plateau (%s %s %s)", rover.getName(),rover.getX(),rover.getY(),rover.getOrientation());
            }

            if(collisionDetected(PlateauGrid.getPlateauGridSingleton(),rover)){
                return String.format("[Moving] %s - %s %s %s [Waring] Collision detected -  %s rover",rover.getName(),rover.getX(),
                        rover.getY(),rover.getOrientation(), PlateauGrid.getPlateauGridSingleton().getGrid()[rover.getX()][rover.getY()]);
            }
        }
        PlateauGrid.getPlateauGridSingleton().setGridItem(rover.getX(),rover.getY(),rover.getName());
        return String.format("[Moving] %s - %s %s %s", rover.getName(),rover.getX(), rover.getY(),rover.getOrientation());
    }

    private static boolean movedWithinGrid(PlateauGrid plateauGrid, Rover rover){
        if(rover.getX() > plateauGrid.getxHeight() || rover.getX() < 0 ){
            return false;
        }else if(rover.getY() > plateauGrid.getyWidth() || rover.getY() < 0){
            return false;
        }

        return  true;
    }

    private static boolean collisionDetected(PlateauGrid plateauGrid, Rover rover){
        return !plateauGrid.getGrid()[rover.getX()][rover.getY()].equals("");
    }
}
