package models;

public class Rover {
    private String name;
    private int x;
    private int y;
    private String orientation; // N S E W
    private String moveSequence;

    public  Rover(){}
    public Rover(String name, int x, int y, String orientation, String moveSequence) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.orientation = orientation;
        this.moveSequence = moveSequence;
    }

    public void executeMoveSequence(){

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public String getMoveSequence() {
        return moveSequence;
    }

    public void setMoveSequence(String moveSequence) {
        this.moveSequence = moveSequence;
    }
}
