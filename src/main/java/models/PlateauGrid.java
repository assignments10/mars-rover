package models;

import java.util.Arrays;

public class PlateauGrid {
    private static volatile PlateauGrid plateauGrid;
    private String[][] grid;
    private int xHeight;
    private int yWidth;

    private PlateauGrid() {}

    //Lazy loading the singleton instance.
    public static PlateauGrid getPlateauGridSingleton(){
        if(plateauGrid == null){
            synchronized (PlateauGrid.class){
                if(plateauGrid == null)
                    plateauGrid = new PlateauGrid();
            }
        }
        return plateauGrid;
    }

    public void init(int height, int width){
        grid = new String[height+1][width+1]; // X | Y
        for(String[] row: grid){
            Arrays.fill(row,"");
        }
        this.xHeight = height;
        this.yWidth = width;
    }

    public String[][] getGrid() {
        return grid;
    }

    public void setGrid(String[][] grid) {
        this.grid = grid;
    }

    public int getxHeight() {
        return xHeight;
    }

    public void setxHeight(int xHeight) {
        this.xHeight = xHeight;
    }

    public int getyWidth() {
        return yWidth;
    }

    public void setyWidth(int yWidth) {
        this.yWidth = yWidth;
    }

    public void setGridItem(int x, int y, String roverName){
        this.grid[x][y] = roverName;
    }

    public String getGridItem(int x, int y){
        return this.grid[x][y];
    }
}
