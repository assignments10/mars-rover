import models.Rover;

public class NavigationRuleEngine {
    private enum Directions{
        N , S, E, W
    }

    public static String turnRover(String currentOrientation, String instruction){
        return switch (Directions.valueOf(currentOrientation)){
            case N -> instruction.equals("L") ? "W" : "E";
            case E -> instruction.equals("L") ? "N" : "S";
            case S -> instruction.equals("L") ? "E" : "W";
            case W -> instruction.equals("L") ? "S" : "N";
        };
    }

    public static Rover executeMove(Rover rover){
        switch (Directions.valueOf(rover.getOrientation())){
            case N -> rover.setY(rover.getY()+1);
            case E -> rover.setX(rover.getX()+1);
            case S -> rover.setY(rover.getY()-1);
            case W -> rover.setX(rover.getX()-1);
        }
        return rover;
    }
}
