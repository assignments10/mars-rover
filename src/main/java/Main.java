import models.PlateauGrid;
import models.Rover;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Util.parsePlateauGridInfo("5 5");

        Map<String, String[]> inputMap = new HashMap<String, String[]>();

        String[] inputArray = new String[2];
        inputArray[0] = "1 2 N";
        inputArray[1] = "LMLMLMLMM";
        inputMap.put("Path Finder", inputArray);

        String[] inputArray2 = new String[2];
        inputArray2[0] = "3 3 E";
        inputArray2[1] = "MMRMMRMRRM";
        inputMap.put("Curiosity", inputArray2);

        String[] inputArray3 = new String[2];
        inputArray3[0] = "3 3 E";
        inputArray3[1] = "MMRMMRMRRM";
        inputMap.put("Soyuz", inputArray3);

        List<Rover> rovers = Util.parseRoverInfo(inputMap);

        for(Rover rover: rovers) {
            System.out.println(Navigator.navigate(PlateauGrid.getPlateauGridSingleton(), rover));
        }
    }
}
